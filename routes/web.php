<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MangasController@index');



/* Routes for Manga model */
Route::get('/mangas/create','MangasController@create')->name('mangas.create');

Route::get('/mangas/{id}/publishers', 'MangasController@show_publisher' )->name('mangas.show_publisher');

Route::get('/mangas/{id}', 'MangasController@show' )->name('mangas.show');

Route::get('/mangas','MangasController@index')->name('mangas.index');

Route::get('/mangas/{id}/edit', 'MangasController@edit')->name('mangas.edit');

Route::put('/mangas/{id}','MangasController@update')->name('mangas.edit');

Route::delete('/mangas/{id}','MangasController@destroy')->name('mangas.destroy');

Route::post('/mangas','MangasController@store')->name('mangas.store');


/* Routes for Publisher model */
Route::get('/publishers/{id}/mangas', 'PublishersController@show_mangas' )->name('publishers.show_mangas');

Route::get('/publishers/{id}', 'PublishersController@show' )->name('publishers.show');

Route::get('/publishers','PublishersController@index')->name('publishers.index');

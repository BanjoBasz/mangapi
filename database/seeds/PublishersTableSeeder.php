<?php

use Illuminate\Database\Seeder;

class PublishersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('publishers')->insert(
                     array(
                             array( //id 1
                                     'name' => 'VIZ Media',
                                     'created_at' =>  DB::raw('now()') ,
                                     'updated_at' =>   DB::raw('now()')

                             ),
                             array(//id 2
                                    'name' => 'Dark Horse Comics',
                                    'created_at' =>  DB::raw('now()'),
                                    'updated_at' =>   DB::raw('now()')
                             ),
                             array(//id 3
                                     'name' => 'Shonen Jump',
                                     'created_at' =>  DB::raw('now()') ,
                                     'updated_at' =>  DB::raw('now()')
                             ),
                     ));

                     

    }
}

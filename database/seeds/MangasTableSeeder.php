<?php

use Illuminate\Database\Seeder;

class MangasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      //Viz Media insert
      DB::table('mangas')->insert(
                     array(
                             array(
                                     'publisher_id' => 1,
                                     'title' => 'One-punch man',
                                     'description' =>'Life gets pretty boring when you can beat the snot out of any villain with just one punch.',
                                     'genre' => 'Comedy | Action ',
                                     'created_at' =>  DB::raw('now()') ,
                                     'updated_at' =>   DB::raw('now()')

                             ),
                             array(
                                     'publisher_id' => 1,
                                     'title' => 'Tokyo Ghoul',
                                     'description' =>'Ghouls live among us, the same as normal people in every way—except for their craving for human flesh.',
                                     'genre' => 'Horror | Action | Thriller ',
                                     'created_at' =>  DB::raw('now()') ,
                                     'updated_at' =>   DB::raw('now()')

                             ),
                             array(
                                     'publisher_id' => 1,
                                     'title' => 'Fullmetal Alchemist',
                                     'description' =>'Alchemy tore the Elric brothers’ bodies apart. Can their bond make them whole again?',
                                     'genre' => 'Comedy | Action | Shonen',
                                     'created_at' =>  DB::raw('now()') ,
                                     'updated_at' =>   DB::raw('now()')

                             )
                     ));


       //Dark Horse Comics insert
       DB::table('mangas')->insert(
                      array(
                              array(
                                      'publisher_id' => 2,
                                      'title' => 'Berserk',
                                      'description' =>'His name is Guts, the Black Swordsman, a feared warrior spoken of only in whispers. Bearer of a gigantic sword, an iron hand, and the scars of countless battles and tortures, his flesh is also indelibly marked with The Brand, an unholy symbol that draws the forces of darkness to him and dooms him as their sacrifice. But Guts won\'t take his fate lying down; he\'ll cut a crimson swath of carnage through the ranks of the damned -- and anyone else foolish enough to oppose him! Accompanied by Puck the Elf, more an annoyance than a companion, Guts relentlessly follows a dark, bloodstained path that leads only to death...or vengeance. Created by Kenturo Miura, Berserk is manga mayhem to the extreme -- violent, horrifying, and mercilessly funny -- and the wellspring for the internationally popular anime series. Not for the squeamish or the easily offended, Berserk asks for no quarter -- and offers none!',
                                      'genre' => 'Action ',
                                      'created_at' =>  DB::raw('now()') ,
                                      'updated_at' =>   DB::raw('now()')

                              ),
                              array(
                                      'publisher_id' => 2,
                                      'title' => 'Fate/Zero',
                                      'description' =>'Gen Urobuchi and Shinjiro’s prequel to Type-Moon’s Fate saga! Based on the hit novel and anime series, Fate/Zero is a faithful manga adaptation that includes never-before-seen content! The fourth Holy Grail War has begun, and seven magi must summon heroes from history to battle to the death. Only one pair can claim the Grail, though—and it will grant them their wishes!',
                                      'genre' => 'Action | Thriller ',
                                      'created_at' =>  DB::raw('now()') ,
                                      'updated_at' =>   DB::raw('now()')
                              ),
                            ));
        //Shonen Jump insert
        DB::table('mangas')->insert(
                       array(
                               array(
                                       'publisher_id' => 3,
                                       'title' => 'Eyeshield 21',
                                       'description' =>'Sena is like any other shy kid starting high school; he\'s just trying to survive. Constantly bullied, he\'s accustomed to running away.

Surviving high school is about to become a lot more difficult after Hiruma, captain of the school\'s American football team, witnesses Sena\'s incredible agility and speed during an escape from some bullies. Hiruma schemes to make Sena the running back of his school team, The Devil Bats, hoping that it will turn around the squad\'s fortunes from being the laughingstock of Japan\'s high school leagues, to title contender.

To protect his precious star player from rivaling recruiters, he enlists Sena as "team secretary," giving him a visored helmet and the nickname "Eyeshield 21" to hide his identity.

The Devilbats will look to make their way to the Christmas Bowl, an annual tournament attended by the best football teams in Japan, with "Eyeshield 21" leading the way. Will they be able to win the Christmas Bowl? Will Sena be able to transform from a timid, undersized freshman to an all-star player? Put on your pads and helmet to find out!',
                                       'genre' => 'Action | Comedy | Shonen',
                                       'created_at' =>  DB::raw('now()') ,
                                       'updated_at' =>   DB::raw('now()')

                               ),
                               array(
                                       'publisher_id' => 3,
                                       'title' => 'Katekyo Hitman Reborn',
                                       'description' =>'There is no putting it lightly—Tsunayoshi Sawada is just no good. He is clumsy, talentless, and desperately in love with the school idol Kyouko Sasagawa, a girl so completely out of his league. Dubbed "Loser Tsuna" by his classmates, he seems to be the very personification of failure in the guise of a middle-schooler.

Tsuna\'s boring life takes an extraordinary twist when he encounters the mysterious Reborn, who happens to be a hitman... and shockingly, a baby! Sent from the strongest Mafia family in Italy, Reborn is assigned the daunting mission of preparing the dull middle schooler to succeed the ninth boss of the notorious Vongola family, who is on the brink of retirement. The dull boy has a grueling road ahead, but with the help of his new criminal affiliates and his peculiar home tutor, perhaps even Loser Tsuna can achieve greatness.',
                                       'genre' => 'Action | Comedy | Shonen ',
                                       'created_at' =>  DB::raw('now()') ,
                                       'updated_at' =>   DB::raw('now()')
                               ),
                             ));
                           }
                         }

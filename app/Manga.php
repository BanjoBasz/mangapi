<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manga extends Model
{

  //Eloquent relationship that a Manga is from a publisher
  public function publisher()
  {
      return $this->belongsTo('App\Publisher');
  }

  //Start Eloquent attributes for setting a attribute when it is not null
  public function setTitleAttribute($title)
  {
       if($title)
       {
         $this->attributes['title'] = $title;
       }
  }

  public function setGenreAttribute($genre)
  {
       if($genre)
       {
         $this->attributes['genre'] = $genre;
       }
  }

  public function setPublisherIdAttribute($publisherId)
  {
       if($publisherId)
       {
         $this->attributes['publisher_id'] = $publisherId;
       }
  }

  public function setDescriptionAttribute($description)
  {
       if($description)
       {
         $this->attributes['description'] = $description;
       }
  }
  //End

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publisher;
use App\Manga;
class PublishersController extends Controller
{

  public function index()
  {
    if(Publisher::all())
    {
      return response()->json(['code' => 200, 'status' => 'success','data' => Publisher::all()],200);
    }

    return response()->json(['code' => 404, 'status' => 'fail','message' => 'No resources'],404);

  }


  public function show($id)
  {
    if(Publisher::find($id))
    {
      return response()->json(['code' => 200, 'status' => 'success','data' => Publisher::find($id)]);
    }

      return response()->json(['code' => 404, 'status' => 'error', 'message' => 'Resource not found'],404);
  }

  public function show_mangas($id)
  {
    if(Publisher::find($id))
    {

       return response()->json(['code' => 200, 'status' => 'success','data' => Publisher::find($id)->mangas ],200);
    }

    return response()->json(['code' => 404, 'status' => 'fail','message' => 'Resource not found'],404);
  }


}

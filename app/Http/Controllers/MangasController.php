<?php

namespace App\Http\Controllers;
use App\Manga;
use Illuminate\Http\Request;

class MangasController extends Controller
{

    public function index()
    {
      if(Manga::all())
      {
        return response()->json(['code' => 200, 'status' => 'success','data' => Manga::all()],200);
      }

      return response()->json(['code' => 404, 'status' => 'fail','message' => 'No resources'],404);

    }


    public function show($id)
    {
      if(Manga::find($id))
      {
        return response()->json(['code' => 200, 'status' => 'success','data' => Manga::find($id)]);
      }

        return response()->json(['code' => 404, 'status' => 'error', 'message' => 'Resource not found'],404);

    }

    public function create()
    {

      return view('mangas.create');
    }

    public function store(Request $request)
    {
       $manga = new Manga();

       $manga->title = $request->title;
       $manga->description = $request->description;
       $manga->genre = $request->genre;
       $manga->publisher_id = $request->publisher;

       try{
         $manga->save();
       }catch(\Illuminate\Database\QueryException $ex)
       {
         return response()->json(['code' => 400 , 'status' => 'fail','message' => $ex->getMessage()],400);
       }

      return response()->json(['code' => 201, 'status' => 'success','data' => $manga],201);
    }

    public function destroy($id)
    {
      if(Manga::find($id))
      {

        Manga::find($id)->delete();

        return response('',204);

      }

      return response()->json(['code' => 404, 'status' => 'fail','message' => 'Resource not found'],404);

    }
    public function show_publisher($id)
    {
      if(Manga::find($id))
      {

         return response()->json(['code' => 200, 'status' => 'success','data' => Manga::find($id)->publisher ],200);
      }


      return response()->json(['code' => 404, 'status' => 'fail','message' => 'Resource not found'],404);
    }

    public function edit($id)
    {
        $manga = Manga::find($id);
        return view('mangas.edit')->with('manga',$manga);
    }


    public function update(Request $request, $id)
    {
      try{
           $manga = Manga::find($id);
           $manga->title = $request->title;
           $manga->description = $request->description;
           $manga->genre = $request->genre;
           $manga->publisher_id = $request->publisher;
           $manga->save();

           return response()->json(['code' => 200, 'status' => 'success','data' => $manga ],200);

     }catch(\Illuminate\Database\QueryException $ex)
     {
        return response()->json(['code' => 400, 'status' => 'fail','message' => $ex->getMessage() ],400);
     }


    }
}

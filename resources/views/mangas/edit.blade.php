<h1> Update Manga <i>"{{$manga->title}}"</i> </h1>

<form method ="POST" action="/mangas/{{$manga->id}}" >
  <!-- Needed for Laravel -->
  {{ csrf_field() }}

  <input name="_method" type="hidden" value="PUT">

  <!-- input name -->
  <label for = "title"> Title </label>
  <input type = "text" id="title" name="title">

  <!-- input email -->
  <label for = "description"> Description </label>
  <input type = "text" id="description" name="description">

  <!-- input password -->
  <label for = "genre"> Genre </label>
  <input type = "text" id="genre" name="genre">

  <!-- input password -->
  <label for = "publisher"> Publisher </label>
  <input type = "number" id="publisher" name="publisher">


  <button type="submit">Update </button>

</form>
